jQuery.noConflict();
jQuery(document).ready(function ($) {
    console.log("yes iam loaded");
    $('<script />', {
        src: "https://npmcdn.com/flatpickr/dist/l10n/de.js"
    }).prependTo('head');
    $('<script />', {
        src: "https://npmcdn.com/flatpickr/dist/flatpickr.min.js"
    }).prependTo('head');
    $('<link />', {
        href: "https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css"
    }).prependTo('head');
    var html = '' +
        '<div class="form-group text-left pull-right" style="float: right;margin-bottom: 14px;min-width: 241px;">\n' +
        '<label for="date">Lalamove Delivery Date & Time:</label>\n' +
        '<input id="date_time_picker" style="width: 100%;" type="text" name="attributes[delivery_datetime]" placeholder="click to select datetime" class="form-control">\n' +
        '</div>';
    /* Check if the page is cart page*/
    if (window.location.href.indexOf("cart") > -1) {
        $("input[name='checkout']").closest("div").before(html);
        $("form[action='/cart']").append('<input type="hidden" class="tbtLalaVal" name="properties[lala_delivery_datetime]" value="">');
        flatpickr('input[name="attributes[delivery_datetime]"]', {
            enableTime: true,
            altInput: true,
            allowInput: false,
            altFormat: 'Y-m-d H:i',
            dateFormat: 'Y-m-dTH:i:S',
            locale: 'en',
            time_24hr: true,
            onChange: function (selectedDates, dateStr, instance) {
                console.log(dateStr);
                $("input[name='properties[lala_delivery_datetime]']").empty().val(dateStr);
            }
        });


        /*$.ajax({
            type: 'GET',
            url: '/cart.js',
            cache: false,
            dataType: 'json',
            success: function (cart) {
                console.log(cart);
            }
        });*/

    }
    if (window.location.href.indexOf("/checkouts/") > -1) {
        console.log("yes found");
        var html = '<input type="hidden" name="checkout[shipping_address][lalamove_date_time]" value="1">';
        $("input[name='checkout[shipping_address][address1]']").before(html);
    }
});
