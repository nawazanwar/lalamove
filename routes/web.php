<?php

use Illuminate\Support\Facades\Route;

Route::get('', ['as' => 'home', 'uses' => 'HomeController@index']);
Route::get('/install', ['as' => 'app.install', 'uses' => 'HomeController@install']);
Route::get('/generate_token', ['as' => 'app.generate_token', 'uses' => 'HomeController@generate_token']);

Route::get('setting/store', ['as' => 'setting.store', 'uses' => 'SettingController@store']);
