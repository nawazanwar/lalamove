<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function store(Request $request)
    {
        $matchCondition = ['shop_id' => $request->get('shop_id')];
        Setting::updateOrCreate($matchCondition, $request->all());
        return response()->json(['status' => 'true']);
    }
}
