<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Shop extends Model
{
    protected $fillable = ['name', 'access_token','hmac'];

    public function settings()
    {
        return $this->hasMany(Setting::class);
    }
}
