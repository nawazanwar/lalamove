@extends('layouts.dashboard')
@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/toastr/toastr.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection

@section('content')
    <section class="content w-auto m-3">
        <form class="form-horizontal" id="setting_form" action="javascript:void(0)"
              enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-12 text-center">
                    <div class="form-group mb-0">
                        <div class="icheck-success d-inline">
                            <input type="checkbox" id="test_mode"
                                   @if(isset($setting->test_mode) AND $setting->test_mode==1)  checked=""
                                   @endif  value="{{ (isset($setting->test_mode) AND $setting->test_mode)?'1':'0' }}">
                            <label for="test_mode">Enable Test Mode</label>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group mb-0">
                        <label for="test_api_key" class=" col-form-label">Test Merchant
                            Id</label>
                        <input type="text" class="form-control form-control-sm" id="test_api_key"
                               value="{{ isset($setting->test_api_key)?$setting->test_api_key:'' }}">
                    </div>
                    <div class="form-group mb-0">
                        <label for="test_api_secret" class=" col-form-label">Test Merchant
                            Secret</label>
                        <input type="text" class="form-control form-control-sm" id="test_api_secret"
                               value="{{ isset($setting->test_api_secret)?$setting->test_api_secret:'' }}">
                    </div>
                    <div class="form-group mb-0">
                        <label for="live_api_key" class=" col-form-label">Live Merchant
                            Id</label>
                        <input type="text" class="form-control form-control-sm" id="live_api_key"
                               value="{{ isset($setting->live_api_key)?$setting->live_api_key:'' }}">
                    </div>
                    <div class="form-group mb-0">
                        <label for="live_api_secret" class=" col-form-label">Live Merchant
                            Secret</label>
                        <input type="text" class="form-control form-control-sm" id="live_api_secret"
                               value="{{ isset($setting->live_api_secret)?$setting->live_api_secret:'' }}">
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group mb-0">
                        <label for="person" class=" col-form-label">Person</label>
                        <input type="text" class="form-control form-control-sm" id="person"
                               value="{{ isset($setting->person)?$setting->person:'' }}">
                    </div>
                    <div class="form-group mb-0">
                        <label for="phone" class=" col-form-label">Phone <small class="text-info"> (Format should be
                                accourding to country)</small></label>
                        <input type="text" class="form-control form-control-sm" id="phone"
                               value="{{ isset($setting->phone)?$setting->phone:'' }}">
                    </div>
                    <div class="form-group mb-0">
                        <label for="contact_address" class=" col-form-label">Address</label>
                        <input type="text" class="form-control form-control-sm" id="address"
                               value="{{ isset($setting->address)?$setting->address:'' }}">
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mb-0">
                                <label for="lat" class=" col-form-label">Latitude</label>
                                <input type="text" class="form-control form-control-sm" id="lat"
                                       value="{{ isset($setting->lat)?$setting->lat:'' }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group mb-0">
                                <label for="lng" class=" col-form-label">Longitude</label>
                                <input type="text" class="form-control form-control-sm" id="lng"
                                       value="{{ isset($setting->lng)?$setting->lng:'' }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group mb-0">
                                <label for="country_id" class=" col-form-label">Country</label>
                                <select class="form-control form-control-sm" id="country_id" name="country_id">
                                    @foreach($countries as $country)

                                        @php
                                            $selected = '';
                                            if (isset($setting->country_id)){
                                                if ($setting->country_id == $country->id){
                                                    $selected = "selected";
                                                }
                                           }
                                        @endphp

                                        <option value="{{ $country->id }}" {{ $selected }}>{{ $country->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group mb-0">
                                <label for="currency" class=" col-form-label">Currency</label>
                                <input type="text" class="form-control form-control-sm" id="currency"
                                       value="{{ isset($setting->currency)?$setting->currency:'' }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 text-right form-group border-top mt-3">
                    <button type="submit" class="btn btn-info btn-sm my-2">Save Changes</button>
                </div>
            </div>
        </form>
    </section>
@stop
@section('scriptInnerFiles')
    <script src="{{ asset('js/exteral.js') }}"></script>
    <script src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
@endsection
@section('pageScript')
    <script>
        ShopifyApp.init({
            apiKey: '{{ config('system.api_key') }}',
            shopOrigin: 'https://{{ $shop->name }}'
        });
        ShopifyApp.ready(function () {
            $('input[type="checkbox"]').click(function () {
                if ($(this).prop("checked") == true) {
                    $(this).val(1);
                } else if ($(this).prop("checked") == false) {
                    $(this).val(0);
                }
            });
            $("#setting_form").submit(function () {
                Ajax.setAjaxHeader();
                $.ajax({
                    type: 'GET',
                    url: "{{ route('setting.store') }}",
                    data: {
                        'country_id': $('#country_id').find('option:selected').val(),
                        'test_api_key': $("#test_api_key").val(),
                        'test_api_secret': $("#test_api_secret").val(),
                        'live_api_key': $("#live_api_key").val(),
                        'live_api_secret': $("#live_api_secret").val(),
                        'test_mode': $("#test_mode").val(),
                        'shop_id': "{{ $shop->id }}",
                        'person': $("#person").val(),
                        'phone': $("#phone").val(),
                        'address': $('#address').val(),
                        'lat': $("#lat").val(),
                        'lng': $("#lng").val()
                    },
                    success: (response) => {
                        if (response.status == 'true') {
                            toastr.success('Setting has been Saved Successfully');
                        }
                    }
                });
            });
        });
    </script>
@stop
