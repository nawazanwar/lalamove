<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <link href="{{ asset('css/vendor.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    @yield('styleInnerFiles')
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
    <div class="content">
        <div class="container-fluid">
            @yield('content')
        </div>
    </div>
</div>
<script src="{{ asset('js/vendor.min.js') }}"></script>
@yield('scriptInnerFiles')
@yield('pageScript')
</body>
</html>
