var Ajax = new function () {
    this.call = function (url, data, method = 'GET', callback) {
        Ajax.setAjaxHeader();
        $.ajax({
            type: method,
            global: false,
            async: true,
            url: url,
            data: data,
            success: function (response) {
                callback(response);
            },
            error: function () {
                console.log("Error Occurred");
            }
        });
    };
    this.setAjaxHeader = function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
            }
        });
    };
};
