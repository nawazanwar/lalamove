<?php
@include "custom/connection.php";
@include "lala/vendor/autoload.php";

$order = file_get_contents('php://input');
$order = json_decode($order, true);
$already_found_order_query = "SELECT * FROM `orders` WHERE `shopify_order_name`='{$order['name']}' AND `shop_id`='{$shop_id}' ";
$already_found_order_result = $conn->query($already_found_order_query);
if ($already_found_order_result->num_rows < 1) {
    $specialRequests = array();
    if (isset($order['gateway']) && $order['gateway'] == 'Cash on Delivery (COD)') {
        array_push($specialRequests, 'COD');
    }
    $remarks_array = array();
    if (isset($order['name']) && $order['name'] != '') {
        array_push($remarks_array, $order['name']);
        if (isset($order['line_items']) && count($order['line_items']) > 0) {
            foreach ($order['line_items'] as $line_item) {
                array_push($remarks_array, $line_item['name'] . " x " . $line_item['quantity']);
            }
        }
    }
    $orderRemarks = implode(', ', $remarks_array);
    $shipping_address = $order['shipping_address'];
    $lang_lat_array = get_lat_long($shipping_address['address1']);
    $totalFee = 0;
    $shipping_lines = $order['shipping_lines'];

    if ($shipping_lines[0]['title'] == 'LalaMove Delivery') {

        $totalFee = $shipping_lines[0]['price'];
        $body = array(
            "serviceType" => "MOTORCYCLE",
            "specialRequests" => $specialRequests,
            "requesterContact" => array(
                "name" => isset($setting['person']) ? $setting['person'] : "",
                "phone" => isset($setting['phone']) ? $setting['phone'] : ""
            ),
            "stops" => array(
                array(
                    "location" =>
                        array(
                            "lat" => isset($setting['lat']) ? $setting['lat'] : "",
                            "lng" => isset($setting['lng']) ? $setting['lng'] : ""
                        ),
                    "addresses" => array(
                        "en_" . $country_code => array(
                            "displayString" => isset($setting['address']) ? $setting['address'] : "",
                            "country" => $country_code
                        )
                    )
                ),
                array(
                    "location" => array(
                        "lat" => trim(isset($lang_lat_array['lat']) ? $lang_lat_array['lat'] : ""),
                        "lng" => trim(isset($lang_lat_array['lng']) ? $lang_lat_array['lng'] : "")
                    ),
                    "addresses" => array(
                        "en_" . $country_code => array(
                            "displayString" => isset($shipping_address['address1']) ? $shipping_address['address1'] : "",
                            "country" => $country_code
                        )
                    )
                )
            ),
            "deliveries" => array(
                array(
                    "toStop" => 1,
                    "toContact" => array(
                        "name" => (isset($shipping_address['name']) && $shipping_address['name'] != '') ? $shipping_address['name'] : $setting['person'],
                        "phone" => (isset($shipping_address['phone']) && $shipping_address['phone'] != '') ? $shipping_address['phone'] : $setting['phone']
                    ),
                    "remarks" => $orderRemarks
                )
            )
        );

        $note_attributes = $order['note_attributes'];
        $delivery_datetime = null;
        if (count($note_attributes) > 0) {
            foreach ($note_attributes as $note_attribute) {
                if ($note_attribute['name'] == 'delivery_datetime' && $note_attribute['value'] != '') {
                    $delivery_datetime = $note_attribute['value'];
                    $body['scheduleAt'] = gmdate('Y-m-d\TH:i:s\Z', strtotime($delivery_datetime));
                }
            }
        }

        $request = new \Lalamove\Api\LalamoveApi($api_url, $api_key, $api_secret, $country_code);

        /* get Quotation Based on Schedule*/

        $quoted_result = $request->quotation($body);
        $quoted_result_status = $quoted_result->getStatusCode();
        $quoted_result_content = $quoted_result->getBody()->getContents();

        if ($quoted_result_status == '200') {
            $quoted_result_content = json_decode($quoted_result_content, true);
            $body['quotedTotalFee'] = array(
                "amount" => $quoted_result_content['totalFee'],
                "currency" => $quoted_result_content['totalFeeCurrency']
            );
        }

        /* Ready to post a Order*/

        $order_result = $request->postOrder($body);
        $order_result_status = $order_result->getStatusCode();
        $order_result_content = $order_result->getBody()->getContents();

        if ($order_result_status == '200') {

            $api_response = json_decode($order_result_content, true);
            /* Ready to Save the Order*/
            $order_query = "INSERT INTO
                            `orders`(
                                    `shop_id`,
                                    `shopify_order_id`,
                                    `shopify_order_name`,
                                    `order_detail`,
                                    `lala_order_id`,
                                    `lala_order_ref`
                                 ) VALUES (
                                    '" . $shop_id . "',
                                    '" . $order['id'] . "',
                                    '" . $order['name'] . "',
                                    '" . json_encode($order) . "',
                                    '" . $api_response['customerOrderId'] . "',
                                    '" . $api_response['orderRef'] . "'
                                 )";
            if ($conn->query($order_query) === TRUE) {
                $last_id = $conn->insert_id;


                $lala_order_info = $request->getOrderStatus($api_response['customerOrderId']);
                $lala_order_info_code = $lala_order_info->getStatusCode();


                if ($lala_order_info_code == '200') {

                    $lala_order_info_response = json_decode($lala_order_info->getBody()->getContents(), true);


                    $update_lala_order_status_query = "UPDATE `orders` SET `lala_order_status`='" . $lala_order_info_response['status'] . "' WHERE `id`=" . $last_id;
                    $conn->query($update_lala_order_status_query);


                    $locations = call_shopify($shop_name, $shopify_token, "/admin/api/2020-04/locations.json", array(), 'GET')['response'];
                    $location_id = json_decode($locations, true)['locations'][0]['id'];

                    /*Ready to create the Order Full Fillment*/

                    $fulfillment_query_params = [
                        "fulfillment" => [
                            'location_id' => $location_id,
                            "tracking_company" => 'Lalamove',
                            "tracking_number" => $api_response['orderRef'],
                            "notify_customer" => true
                        ]
                    ];


                    $fulfillment_url = "/admin/api/2020-04/orders/" . $order['id'] . "/fulfillments.json";
                    $fulfillment_encode = call_shopify($shop_name, $shopify_token, $fulfillment_url, $fulfillment_query_params, 'POST')['response'];
                    $fulfillment_decode = json_decode($fulfillment_encode, true);
                    if (isset($fulfillment_decode['fulfillment'])) {
                        $update_shopify_fullfillment_query = "UPDATE `orders` SET `shopify_fulfillment_id`='" . $fulfillment_decode['fulfillment']['id'] . "' WHERE `id`=" . $last_id;
                        $conn->query($update_shopify_fullfillment_query);
                    }
                }
            }
        }//end of order status

    }//end of the check (method is Lalamove Delivery)

}//end of the condition
