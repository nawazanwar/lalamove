<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $countries = array
        (
            'HK' => '🇭🇰 Hong Kong',
            'ID' => '🇮🇩 Indonesia',
            'IN' => '🇮🇳 India',
            'MY' => '🇲🇾 Malaysia',
            'PH' => '🇵🇭 Philippines',
            'SG' => '🇸🇬 Singapore',
            'TH' => '🇹🇭 Thailand',
            'VN' => '🇻🇳 Viet Nam'
        );

        DB::table('countries')->delete();

        foreach ($countries as $code => $name) {
            DB::table('countries')->insert([
                'code' => $code,
                'name' => $name,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
        }
    }
}
