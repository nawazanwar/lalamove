<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Setting;
use App\Models\Country;
use App\Models\Shop;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->truncate();
        $modal = new Setting();
        $modal->country()->associate(Country::whereCode('SG')->first());
        $modal->shop()->associate(Shop::whereId("1")->first());
        $modal->test_api_key = "38b07ae8b05848a08665f26be572e281";
        $modal->test_api_secret = "MCwCAQACBQDYJieHAgMBAAECBCTg3WECAwDjnwIDAPMZAgIWFQICF1ECAwCj";
        $modal->test_mode = 1;
        $modal->person = 'Developer Maxenius';
        $modal->phone = '+6592344758';
        $modal->address = '1 Raffles Place #04-00, One Raffles Place Shopping Mall, Singapore';
        $modal->lat = '1.284318';
        $modal->lng = '103.851335';
        $modal->currency = 'SGD';
        $modal->save();
    }
}
