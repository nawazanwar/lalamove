<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('shops')) {

            return true;

        }

        Schema::create('shops', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->text('hmac')->nullable();
            $table->text('access_token')->nullable();
            $table->string('active_theme_id')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shops');
    }
}
