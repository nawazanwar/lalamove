<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('shop_id')->unsigned()->nullable();
            $table->string('shopify_order_id')->nullable();
            $table->string('shopify_order_name')->nullable();
            $table->longText('order_detail')->nullable();
            $table->string('lala_order_id')->nullable();
            $table->string('lala_order_ref')->nullable();
            $table->string('lala_order_status')->nullable();
            $table->string('shopify_fulfillment_id')->nullable();
            $table->string('shopify_fulfillment_status')->nullable();
            $table->timestamps();
            $table->foreign('shop_id')
                ->references('id')
                ->on('shops')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
